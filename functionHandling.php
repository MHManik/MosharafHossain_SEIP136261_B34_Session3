<?php
//Float Val Functions- Which collect/extract float value.

$a='3.567acore';

$b=floatval($a);

echo $b;

echo "</br>";
echo "</br>";
?>

<?php
//empty funtion test
$a='';
$b=0;
$c=Null;
$d="asdf";

$x=empty($a);
$y=empty($b);
$z=empty($c);
$r=empty($d);

echo $x."  ".$y."  ".$z. "  ".$r;

echo "</br>";
echo "</br>";
?>

<?php
// is_array function test
$a=['x','y','z'];
$b=is_array($a);
echo $b;
echo "</br>";
echo "</br>";
?>

<?php
// is_null function test
$a='';
$b=0;
$c=Null;

$x=is_null($a);
$y=is_null($b);
$z=is_null($c);

echo $x." --  ".$y."  -- ".$z;

echo "</br>";
echo "</br>";

?>
<?php
// isset function test
$a=0;

if (isset($a)){
    echo " $a is set and Not Null";
}

echo "</br>";
echo "</br>";
?>

<?php
// is__object function test

$a="asdf";

$obj=(object) array("a","b","c");

if (is_object($a)){
    echo "Yes it is object";

}
else
    echo "it is not object";
echo "</br>";
if (is_object($obj)){
    echo "Yes it is object";
}
else
    echo "it is not object";
echo "</br>";
echo "</br>";
?>

<?php
//prin_r function test
$obj= array("a","b","c");
print_r($obj);

echo "</br>";
echo "</br>";
?>

<?php
//unset function test
$a="sonmthing";
unset($a);
var_dump($a);

echo "</br>";
echo "</br>";
?>

<?php
//var_dump function test
$x= array("a","b","c");
var_dump($x);
echo "</br>";
echo "</br>";
?>

<?php
//gettype function test
$a=12;
$b="asdf";

$c=gettype($a);
$d=gettype($b);

echo $c;
echo "</br>";
echo $d;
echo "</br>";
echo "</br>";
?>

<?php
//is_bool function test
$a=true;
$b=is_bool($a);

if ($b=1){
    echo "Yes it is bool";
}
else
    echo "it is not boot";

echo "</br>";
echo "</br>";
?>
<?php
//serialised function test
$serialised_data = serialize(array("Math","English","Bangla" ));
echo $serialised_data;
echo "</br>";

echo serialize(round(96.670000000000002, 2));
echo "</br>";
echo serialize(96);


echo "</br>";
?>

<?php
//unserialised function test
$arr=serialize(array("a","b","c"));

echo $arr;
echo '<pre>';
print_r(unserialize($arr));
echo '</pre>';
echo "</br>";
echo "</br>";
?>

<?php
//boolval test function test
echo '--------';
$a=NULL;
if(boolval($a)){
    echo "true";
}else{
    echo "false";
}
echo "</br>";
echo "</br>";
?>

<?php
//intval test function test

$a=6;
if(intval($a)){
    echo "true";
}else{
    echo "false";
}
echo "</br>";
echo "</br>";
?>

<?php
//is_scalar function test

$a="something";

$b=is_scalar($a);
if ($b==1){
    echo "Yes it is String and Scalar type ";
}else{
    echo "No, It is not string or scalar type";

}

